var express = require('express');
var router = express.Router();

/* GET chat room page. */
router.get('/', function(req, res, next) {
    res.render('chatroom', { title: 'Express Chat' });
});

module.exports = router;