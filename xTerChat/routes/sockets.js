var io = require('socket.io');

exports.initialize = function(server) {
    io = io.listen(server);
    var self = this;
    this.chatInfra = io.of('/chat_infra');
    this.chatInfra.on("connection", function(socket) {
        socket.on('set_name', function(data) {
            socket.nickname = data.name;
            console.log(socket.nickname);
            socket.emit('name_set', data);
            socket.send(JSON.stringify({
                type: "serverMessage",
                message: 'Welcome to the most interesting chat room on earth!'
            }));
        });
        socket.on("join_room", function(room) {
            socket.join(room.name);
            console.log(socket.nickname + "is join " + room.name);
            var comSocket = self.chatCom.sockets[socket.id];
            comSocket.join(room.name);
            comSocket.room = room.name;
            socket.in(room.name).broadcast
                .emit('user_entered', { 'name': socket.nickname });

        });
        // socket.on('get_rooms', function() {
        //     var rooms = {};
        //     for (var room in io.sockets.adapter.rooms) {
        //         if (room.indexOf("/chat_infra") == 0) {
        //             var roomName = room.replace("/chat_infra/", "");
        //             rooms[roomName] = io.sockets.adapter.rooms[room].length;
        //         };
        //     }
        //     socket.emit("rooms_list", rooms);
        // });
    });
    this.chatCom = io.of("/chat_com");
    this.chatCom.on("connection", function(socket) {
        socket.on('message', function(message) {
            message = JSON.parse(message);
            if (message.type == "userMessage") {
                socket.on('nickname', function(err, nickname) {
                    message.username = nickname;
                    socket.in(socket.room).broadcast.send(JSON.stringify(message));
                    message.type = "myMessage";
                    socket.send(JSON.stringify(message));
                });
            }
        });
    });
};